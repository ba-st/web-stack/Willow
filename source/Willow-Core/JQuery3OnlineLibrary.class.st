"
I'm a WALibrary serving the files from the official CDN.
"
Class {
	#name : #JQuery3OnlineLibrary,
	#superclass : #WAOnlineLibrary,
	#category : #'Willow-Core-Libraries'
}

{ #category : #Updating }
JQuery3OnlineLibrary >> updateRoot: aRoot [

	aRoot javascript
		url: ( 'https://code.jquery.com/jquery-<1s>.min.js' expandMacrosWith: self version );
		anonymousSubResourceIntegrity: 'sha384-vtXRMe3mGCbOeY7l30aIg8H9p3GdeSe4IFlP6G8JMa7o7lXvnz3GFKzPxzJdPfGK'
]

{ #category : #Accessing }
JQuery3OnlineLibrary >> version [

	^ '3.6.0'
]
